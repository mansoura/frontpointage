import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { AuthentificationService } from './authentification.service';

@Injectable({
  providedIn: 'root'
})
export class PointageService {

  constructor(private http: HttpClient,private authService: AuthentificationService) { }

  private API_URL = "http://localhost:9100"; 

  getAll(){
    let headers = new HttpHeaders({'authorization':'Bearer '+this.authService.jwt});

    return this.http.get(this.API_URL+'/pointage/getAll',{headers:headers,});
  }

  getbyEmpName(employerName){
    let headers = new HttpHeaders({'authorization':'Bearer '+this.authService.jwt});
    return this.http.get(this.API_URL+'/pointage/getByEmployerName/'+employerName,{headers:headers,});
  }
}
