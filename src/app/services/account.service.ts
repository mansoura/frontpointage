import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  public API_URL = "http://localhost:9100"; 

  constructor(private http: Http) { }

  createUser(user){
      user.role = 'ADMIN';
      return this.http.post(this.API_URL+"/register", user); 
  }

}
