import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';


@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  public jwt: string ="";
  public username: string="";
  public role: string="";
  public user: any;

  public API_URL = "http://localhost:9100"; 


  constructor(private http:HttpClient) { }

  login(data){
    return this.http.post(this.API_URL+"/login", data, {observe:'response'})
  }

  saveToken(jwt: string){
    localStorage.setItem('token', jwt);
    this.jwt = jwt;
    this.parseJWT();
  }

  parseJWT(){
    let jwtHelper = new JwtHelperService();
    let jwtObject = jwtHelper.decodeToken(this.jwt)
    console.log(jwtObject)

    this.username = jwtObject.sub;
    this.role = jwtObject.roles[0];
    console.log(this.role)
    console.log(this.username)

  }

  isAdmin(){
    return this.role.indexOf('ADMIN')>=0;
  }

  isAuthenticated(){
    return this.role && (this.isAdmin())
  }

  loadToken(){
    this.jwt = localStorage.getItem('token');
    this.parseJWT();
  }

  logOut(){
    localStorage.removeItem('token');
    this.initParams();
  }

  initParams(){
    this.jwt = "";
    this.username = "";
    this.role = "";
  }
}
