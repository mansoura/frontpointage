import { Component, OnInit } from '@angular/core';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private accountService: AccountService) { }

  public errorMessage: String="";

  ngOnInit() {
  }

  registration(data){
    this.accountService.createUser(data).subscribe(resp=>{
      confirm("inscription réussi");
    }, err => {
        console.log(err);
        this.errorMessage = "username already exist";
      }) 
  }

}
