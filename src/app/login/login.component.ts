import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from 'src/app/services/authentification.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
  errorMessage: String;

  constructor(private authService: AuthentificationService, private router: Router) { }

  ngOnInit() {
  }

  onLogin(data){
    this.authService.login(data).subscribe(resp => {
      let jwt = resp.headers.get('Authorization');
      this.authService.saveToken(jwt);
      this.router.navigateByUrl("/pointage");
    }, err => {
      console.log(err);
      this.errorMessage = "username ou mot de passe incorrect !";
    })
  }

}

