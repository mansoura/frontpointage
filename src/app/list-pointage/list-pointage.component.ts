import { Component, OnInit } from '@angular/core';
import { PointageService } from '../services/pointage.service';

@Component({
  selector: 'app-list-pointage',
  templateUrl: './list-pointage.component.html',
  styleUrls: ['./list-pointage.component.css']
})
export class ListPointageComponent implements OnInit {

  pointages;
  keyword: String = "";

  constructor(private pointageService: PointageService) { }

  ngOnInit() {
    this.showList();
  }

  showList(){
    this.pointageService.getAll().subscribe(resp => {
      this.pointages = resp;
    })  
  }

  doSearch(){
    if(this.keyword === ""){
      this.showList();
    }else{
      this.pointageService.getbyEmpName(this.keyword).subscribe(resp =>{
        this.pointages = resp;
      })
    }
  }

}
